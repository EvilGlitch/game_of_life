#include <stdbool.h>


typedef struct GameOfLife
{
	unsigned int rows;
	unsigned int columns;
	unsigned int alive_cells;
	bool* population;
} GameOfLife;


void evolve(GameOfLife* game_of_life);
int runLenghtDecoder(GameOfLife* game_of_life, char* generator);
void clear(GameOfLife* game_of_life);
void fill(GameOfLife* game_of_life);
void randomize(GameOfLife* game_of_life);




