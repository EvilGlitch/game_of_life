#include <stdlib.h>
#include <ctype.h>
#include <time.h>
#include "game_of_life.h"


static int modulo(int a, int n);
static int aliveNeighbors(GameOfLife* game_of_life, int x, int y);


static int modulo(int a, int n)
{
	return (a%n+n)%n;
}


static int aliveNeighbors(GameOfLife* game_of_life, int x, int y)
{
	int alive_neighbors=0;
	for(int i=x-1; i<=x+1; i++)
	for(int j=y-1; j<=y+1; j++)
	{
		if(game_of_life->population[modulo(i,game_of_life->rows)*game_of_life->columns+modulo(j,game_of_life->columns)])
		{
			alive_neighbors++;
		}
	}
	if(game_of_life->population[x*game_of_life->columns+y])
	{
		alive_neighbors--;
	}
	return alive_neighbors;
}


void evolve(GameOfLife* game_of_life)
{
	int alive_neighbors;
	bool* next_generation = (bool*)malloc(game_of_life->rows*game_of_life->columns*sizeof(bool));
	game_of_life->alive_cells=0;
	for(int i=0; i<game_of_life->rows; i++)
	for(int j=0; j<game_of_life->columns; j++)
	{
		alive_neighbors=aliveNeighbors(game_of_life, i, j);
		if((next_generation[i*game_of_life->columns+j]=(alive_neighbors==3 || (alive_neighbors==2 && game_of_life->population[i*game_of_life->columns+j]))))
		{
			game_of_life->alive_cells++;
		}

	}
	free(game_of_life->population);
	game_of_life->population=next_generation;
}


void clear(GameOfLife* game_of_life)
{
	for(int i=0; i<game_of_life->rows; i++)
	for(int j=0; j<game_of_life->columns; j++)
	{
		game_of_life->population[i*game_of_life->columns+j]=false;
	}
	game_of_life->alive_cells=0;
}


void fill(GameOfLife* game_of_life)
{
	for(int i=0; i<game_of_life->rows; i++)
	for(int j=0; j<game_of_life->columns; j++)
	{
		game_of_life->population[i*game_of_life->columns+j]=true;
	}
	game_of_life->alive_cells=game_of_life->rows*game_of_life->columns;
}


void randomize(GameOfLife* game_of_life)
{
	game_of_life->alive_cells=0;
	srand(time(NULL));
	for(int i=0; i<game_of_life->rows; i++)
	for(int j=0; j<game_of_life->columns; j++)
	{
		if((game_of_life->population[i*game_of_life->columns+j]=((double)rand()/(double)((unsigned)RAND_MAX+1))<0.5))
		{
			game_of_life->alive_cells++;
		}
	}
}


int runLenghtDecoder(GameOfLife* game_of_life, char* generator)
{
	char current[] = "X";
	bool value;
	int count=0;
	int input_position=0;
	int output_position=0;
	game_of_life->alive_cells=0;
	while((*current=generator[input_position++])!='\0')
	{
		if(isdigit(*current))
		{
			count=10*count+atoi(current);
		}
		else if (isalpha(*current))
		{
			if(count==0)
			{
				count=1;
			}
			if(output_position+count>game_of_life->rows*game_of_life->columns)
			{
				return 0;
			}
			switch(*current)
			{
				case 'a':
				game_of_life->alive_cells+=count;
				value=true;
				break;
				case 'd':
				value=false;
				break;
				default:
				return 0;
				break;
			}
			while(count>0)
			{
				game_of_life->population[output_position++]=value;
				count--;
			}
		}
	}
	if(output_position!=game_of_life->rows*game_of_life->columns)
	{
		return 0;
	}
	return 1;
}
