#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "game_of_life.h"

#define WINDOW_TITLE "Game of Life"


typedef struct Simulation
{
	GameOfLife game_of_life;
	bool paused;
	unsigned int evolution_rate;
} Simulation;


static GLboolean compileShader(const GLchar* src, GLenum shaderType, GLuint *shader);
static GLboolean linkProgram(GLuint* program);
static GLuint generateVBO(unsigned int rows, unsigned int columns);
static void updateEVO(Simulation* simulation, GLuint* ebo);
static void fpsCounter(GLFWwindow* window);
static void framebufferSizeCallback(GLFWwindow* window, int width, int height);
static void windowSizeCallback(GLFWwindow* window, int width, int height);
static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
static void cursorPositionCallback(GLFWwindow* window, double xpos, double ypos);
static void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods);


const GLchar* VERTEX_SHADER_SOURCE = "#version 330 core\n"
"layout (location = 0) in vec3 coord3d;\n"
"void main()\n"
"{\n"
"   gl_Position = vec4(coord3d.x, coord3d.y, coord3d.z, 1.0);\n"
"}\n\0";
const GLchar* FRAGMENT_SHADER_SOURCE = "#version 330 core\n"
"out vec4 FragColor;\n"
"uniform vec4 color;\n"
"void main()\n"
"{\n"
"   FragColor = color;\n"
"}\n\0";


int main(int argc, char **argv)
{
	Simulation simulation;
	unsigned int width,height;

	// define arguments
	static struct option long_options[] =
	{
		{"width", required_argument, 0, 'w'},
		{"height", required_argument, 0, 'h'},
		{"rows", required_argument, 0, 'r'},
		{"columns", required_argument, 0, 'c'},
		{"evolution", required_argument, 0, 'e'},
		{"data", optional_argument, 0, 'd'},
		{0, 0, 0, 0}
	};

	// read arguments
	int c;
	int check=0;
	char* cell_values=NULL;
	while ((c = getopt_long(argc, argv, "w:h:r:c:e:d::", long_options, NULL)) != -1) {
		switch (c) {
			case 'w':
			width=atoi(optarg);
			check++;
			break;
			case 'h':
			height=atoi(optarg);
			check++;
			break;
			case 'r':
			simulation.game_of_life.rows=atoi(optarg);
			check++;
			break;
			case 'c':
			simulation.game_of_life.columns=atoi(optarg);
			check++;
			break;
			case 'e':
			simulation.evolution_rate=atoi(optarg);
			check++;
			break;
			case 'd':
			cell_values=optarg;
			break;
			case ':':
			case '?':
			default:
			exit(EXIT_FAILURE);
			break;
		}
	}

	// check that all mandatory arguments have been introduced
	if(check!=5)
	{
		fprintf(stderr, "Missing arguments\n");
		exit(EXIT_FAILURE);
	}

	// some initializations
	simulation.paused=false;
	simulation.game_of_life.population=(bool*)malloc(simulation.game_of_life.rows*simulation.game_of_life.columns*sizeof(bool));

	// if initial state has been introduced as an argument use it in other case clear the board
	if(cell_values!=NULL)
	{
		if(!runLenghtDecoder(&simulation.game_of_life, cell_values))
		{
			fprintf(stderr, "Encoding error\n");
			exit(EXIT_FAILURE);
		}
	}
	else
	{
		clear(&simulation.game_of_life);
	}

	// glfw initializations
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	GLFWwindow* window = glfwCreateWindow(width, height, WINDOW_TITLE, NULL, NULL);
	if (window == NULL)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	// set glfw callbacks
	glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);
	glfwSetWindowSizeCallback(window, windowSizeCallback);
	glfwSetKeyCallback(window, keyCallback);
	glfwSetCursorPosCallback(window, cursorPositionCallback);
	glfwSetMouseButtonCallback(window, mouseButtonCallback);

	// set user pointer to be able to access to the simulation where in a callback
	glfwSetWindowUserPointer(window, &simulation);

	glfwMakeContextCurrent(window);

	// load all OpenGL function pointers
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		exit(EXIT_FAILURE);
	}

	// build and compile shaders
	// vertex shader
	GLuint vertexShader;
	if(!compileShader(VERTEX_SHADER_SOURCE, GL_VERTEX_SHADER, &vertexShader))
	{
		exit(EXIT_FAILURE);
	}
	// fragment shader
	GLuint fragmentShader;
	if(!compileShader(FRAGMENT_SHADER_SOURCE, GL_FRAGMENT_SHADER, &fragmentShader))
	{
		exit(EXIT_FAILURE);
	}

	// link shaders
	GLuint shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	if(!linkProgram(&shaderProgram))
	{
		exit(EXIT_FAILURE);
	}

	// delete used shaders
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	// set color of the cells
	glUseProgram(shaderProgram);
	int vertexColorLocation = glGetUniformLocation(shaderProgram, "color");
	glUniform4f(vertexColorLocation, 1.0f, 1.0f, 1.0f, 1.0f);

	// generate vao, vbo and ebo
	GLuint vao,vbo,ebo;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	vbo=generateVBO(simulation.game_of_life.rows, simulation.game_of_life.columns);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3*sizeof(GLfloat), (void*)0);
	glEnableVertexAttribArray(0);
	glGenBuffers(1, &ebo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	double current_time;
	double last_time=0;
	// render loop
	while (!glfwWindowShouldClose(window))
	{
		current_time = glfwGetTime();

		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// update buffer with the current generation
		updateEVO(&simulation, &ebo);

		// draw
		glUseProgram(shaderProgram);
		glBindVertexArray(vao);
		glDrawElements(GL_TRIANGLES, simulation.game_of_life.alive_cells*6, GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);

		// evolve if the simulation is not paused and enough time has elapsed since last evolution
		if(!simulation.paused && current_time-last_time>=1.0/simulation.evolution_rate)
		{
			last_time = current_time;
			evolve(&simulation.game_of_life);
		}

		// swap buffers
		glfwSwapBuffers(window);
		glfwPollEvents();

		// print fps in window title
		fpsCounter(window);
	}

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// deallocate resources
	glDeleteVertexArrays(1, &vao);
	glDeleteBuffers(1, &vbo);
	glDeleteBuffers(1, &ebo);

	glfwTerminate();
	exit(EXIT_SUCCESS);
}


GLboolean compileShader(const GLchar* src, GLenum shaderType, GLuint *shader){
	*shader = glCreateShader(shaderType);
	glShaderSource(*shader, 1, &src, NULL);
	glCompileShader(*shader);
	// check for shader compile errors
	GLint success;
	glGetShaderiv(*shader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		GLint log_length;
		glGetShaderiv(*shader, GL_INFO_LOG_LENGTH, &log_length);
		char info_log[log_length];
		glGetShaderInfoLog(*shader, log_length, NULL, info_log);
		return GL_FALSE;
	}
	return GL_TRUE;
}


GLboolean linkProgram(GLuint* program)
{
	glLinkProgram(*program);
	// check for linking errors
	GLint success;
	glGetProgramiv(*program, GL_LINK_STATUS, &success);
	if (!success)
	{
		GLint log_length;
		glGetProgramiv(*program, GL_INFO_LOG_LENGTH, &log_length);
		char info_log[log_length];
		glGetProgramInfoLog(*program, log_length, NULL, info_log);
		return GL_FALSE;
	}
	return GL_TRUE;
}


GLuint generateVBO(unsigned int rows, unsigned int columns)
{
	// create mesh
	GLfloat* mesh=(GLfloat*)malloc((rows+1)*(columns+1)*3*sizeof(GLfloat));
	GLfloat cell_width=2.0/columns;
	GLfloat cell_height=2.0/rows;
	for(int i=0; i<=rows; i++)
	for(int j=0; j<=columns; j++)
	{
		mesh[i*(columns+1)*3+j*3+0]=j*cell_width-1;
		mesh[i*(columns+1)*3+j*3+1]=-i*cell_height+1;
		mesh[i*(columns+1)*3+j*3+2]=0;
	}
	// buffer mesh in vbo
	GLuint vbo;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, (rows+1)*(columns+1)*3*sizeof(GLfloat), mesh, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	free(mesh);
	return vbo;
}


void updateEVO(Simulation* simulation, GLuint* ebo)
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, simulation->game_of_life.rows*simulation->game_of_life.columns*6*sizeof(GLuint), NULL, GL_STATIC_DRAW);
	GLuint* buffer = (GLuint*)glMapBufferRange(GL_ELEMENT_ARRAY_BUFFER, 0, simulation->game_of_life.alive_cells*6*sizeof(GLuint) , GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
	int currentPosition=0;
	for(int i=0; i<simulation->game_of_life.rows; i++)
	for(int j=0; j<simulation->game_of_life.columns; j++)
	{
		if(simulation->game_of_life.population[i*simulation->game_of_life.columns+j])
		{
			buffer[currentPosition++]=i*(simulation->game_of_life.columns+1)+j;
			buffer[currentPosition++]=(i+1)*(simulation->game_of_life.columns+1)+j;
			buffer[currentPosition++]=(i+1)*(simulation->game_of_life.columns+1)+(j+1);
			buffer[currentPosition++]=i*(simulation->game_of_life.columns+1)+j;
			buffer[currentPosition++]=i*(simulation->game_of_life.columns+1)+(j+1);
			buffer[currentPosition++]=(i+1)*(simulation->game_of_life.columns+1)+(j+1);
		}
	}
	glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}


static void framebufferSizeCallback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}


static void windowSizeCallback(GLFWwindow* window, int width, int height)
{
	glfwSetWindowSize(window, width, height);
}


static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if(action==GLFW_PRESS)
	{
		Simulation* simulation = (Simulation*)glfwGetWindowUserPointer(window);
		static int windowed_xpos,windowed_ypos,windowed_width,windowed_height;
		switch(key)
		{
			case GLFW_KEY_SPACE:
			simulation->paused=!simulation->paused;
			break;
			case GLFW_KEY_RIGHT:
			if(simulation->paused)
			{
				evolve(&simulation->game_of_life);
			}
			break;
			case GLFW_KEY_UP:
			simulation->evolution_rate++;
			break;
			case GLFW_KEY_DOWN:
			if(simulation->evolution_rate>0)
			{
				simulation->evolution_rate--;
			}
			break;
			case GLFW_KEY_C:
			if(simulation->paused)
			{
				clear(&simulation->game_of_life);
			}
			break;
			case GLFW_KEY_F:
			if(simulation->paused)
			{
				fill(&simulation->game_of_life);
			}
			break;
			case GLFW_KEY_R:
			if(simulation->paused)
			{
				randomize(&simulation->game_of_life);
			}
			break;
			case GLFW_KEY_ESCAPE:
			glfwTerminate();
			exit(EXIT_SUCCESS);
			break;
			case GLFW_KEY_F11:
			if (glfwGetWindowMonitor(window)!=NULL)
			{
				glfwSetWindowMonitor(window, NULL, windowed_xpos, windowed_ypos, windowed_width, windowed_height, 0);
			}
			else
			{
				GLFWmonitor* monitor = glfwGetPrimaryMonitor();
				if (monitor!=NULL)
				{
					const GLFWvidmode* mode = glfwGetVideoMode(monitor);
					glfwGetWindowPos(window, &windowed_xpos, &windowed_ypos);
					glfwGetWindowSize(window, &windowed_width, &windowed_height);
					glfwSetWindowMonitor(window, monitor, 0, 0, mode->width, mode->height, mode->refreshRate);
				}
			}
			break;
			default:
			break;
		}
	}
}


static void cursorPositionCallback(GLFWwindow* window, double xpos, double ypos)
{
	int width,height;
	glfwGetWindowSize(window, &width, &height);
	if(xpos>=0 && ypos>=0 && xpos<=width && ypos<=height)
	{
		int right_button = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT);
		int left_button = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
		if(left_button!=right_button)
		{
			Simulation* simulation = (Simulation*)glfwGetWindowUserPointer(window);
			double cell_width=(double)width/simulation->game_of_life.columns;
			double cell_height=(double)height/simulation->game_of_life.rows;
			int position = ((int)(ypos/cell_height))*simulation->game_of_life.columns+(int)(xpos/cell_width);
			if (left_button==GLFW_PRESS && !simulation->game_of_life.population[position])
			{
				simulation->game_of_life.population[position]=true;
				simulation->game_of_life.alive_cells++;
			}
			else if (right_button==GLFW_PRESS && simulation->game_of_life.population[position])
			{
				simulation->game_of_life.population[position]=false;
				simulation->game_of_life.alive_cells--;
			}
		}
	}
}


static void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
{
	int width,height;
	glfwGetWindowSize(window, &width, &height);
	double xpos, ypos;
	glfwGetCursorPos(window, &xpos, &ypos);
	if (action==GLFW_PRESS && xpos>=0 && ypos>=0 && xpos<=width && ypos<=height)
	{
		Simulation* simulation = (Simulation*)glfwGetWindowUserPointer(window);
		double cell_width=(double)width/simulation->game_of_life.columns;
		double cell_height=(double)height/simulation->game_of_life.rows;
		int position = ((int)(ypos/cell_height))*simulation->game_of_life.columns+(int)(xpos/cell_width);
		if(button==GLFW_MOUSE_BUTTON_LEFT && !simulation->game_of_life.population[position])
		{
			simulation->game_of_life.population[position]=true;
			simulation->game_of_life.alive_cells++;
		}
		else if(button==GLFW_MOUSE_BUTTON_RIGHT && simulation->game_of_life.population[position])
		{
			simulation->game_of_life.population[position]=false;
			simulation->game_of_life.alive_cells--;
		}
	}
}


void fpsCounter(GLFWwindow* window)
{
	static double last_time=0;
	static int frames=0;

	double current_time=glfwGetTime();
	double delta=current_time-last_time;
	frames++;

	// calculate fps only when at least one second has elapsed
	if(delta>=1.0)
	{
		char title[255];
		snprintf(title, 255, "%s [FPS: %3.2f]", WINDOW_TITLE, frames/delta);
		glfwSetWindowTitle (window, title);
		frames=0;
		last_time=current_time;
	}
}
