DIRS=bin/ tmp/ tmp/external external/lib
CC=gcc
CFLAGS=-O3
LDLIBS=-lglad -lglfw -ldl
SOURCES=$(wildcard src/*.c)
OBJECTS=$(SOURCES:src/%.c=tmp/%.o)
EXECUTABLE_NAME=game_of_life
INCLUDE=external/include
LIBRARIES=external/lib

default: bin/$(EXECUTABLE_NAME)
    
bin/$(EXECUTABLE_NAME): $(OBJECTS) $(LIBRARIES)/libglad.a
	$(CC) $(OBJECTS) -L $(LIBRARIES) $(LDLIBS) -o $@

tmp/%.o: src/%.c
	$(CC) -c -I $(INCLUDE) $(CFLAGS) $< -o $@

$(LIBRARIES)/lib%.a: tmp/external/%.o
	ar rcs $@ $<

tmp/external/%.o: external/src/%.c
	$(CC) -c -I $(INCLUDE) $(CFLAGS) $< -o $@

clean:
	rm -fr tmp/*
	rm -fr bin/*

$(shell mkdir -p $(DIRS))
