# Game of Life

Conway's Game of Life implemented in C using OpenGL.


## Dependencies

* [glfw](http://www.glfw.org/) (Version 3.2 or above)
* [glad](https://github.com/Dav1dde/glad) (Included in `external` directory)

## Building

Linux:

```
make
```

## Usage


### Launching

```
game_of_life -h WINDOW_HEIGHT -w WINDOW_WIDTH -r NUMBER_OF_ROWS -c NUMBER_OF_COLUMNS -e EVOLUTION_RATE [--data="RUN_LENGHT_ENCODED_SEED"]
```

The `--data` option allows to initialize the board with the desired values. The string provided must contain the values of all the cells run-lenght encoded by rows. This encoding is simple, the cells can be in two diferent states each one represented by its initial letter.

| State | Letter |
|-------|--------|
| alive | a      |
| dead  | d      |

By prefixing a number *n* before one of this letters the letter is repeated *n* times. If no number if prefixed the letter is only repeated one time. An initial value must be given to every cell in the board otherwise an error will be thrown. Some popular patters can be found in the directory `examples`.

### Controls

| Key                           | Action                                         |
|-------------------------------|------------------------------------------------|
| <kbd>SPACE</kbd>              | Pause/Resume                                   |
| <kbd>RIGHT</kbd>              | Advance one generation (while paused)          |
| <kbd>UP</kbd>                 | Increase the evolution rate by one             |
| <kbd>DOWN</kbd>               | Decrease the evolution rate by one (minimum 0) |
| <kbd>C</kbd>                  | Clear the board (while paused)                 |
| <kbd>F</kbd>                  | Fill the board (while paused)                  |
| <kbd>R</kbd>                  | Randomize the board (while paused)             |
| <kbd>F11</kbd>                | Toggle fullscreen                              |
| <kbd>LEFT MOUSE BUTTON</kbd>  | Change cell state to alive                     |
| <kbd>RIGHT MOUSE BUTTON</kbd> | Change cell state to dead                      |
| <kbd>SCAPE</kbd>              | Exit                                           |

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

